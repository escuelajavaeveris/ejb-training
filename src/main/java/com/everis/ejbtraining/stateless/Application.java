package com.everis.ejbtraining.stateless;

import com.everis.ejbtraining.stateless.model.BookingCar;
import com.everis.ejbtraining.stateless.service.BookingCarService;
import junit.framework.Assert;

import javax.ejb.embeddable.EJBContainer;
import javax.naming.NamingException;

/**
 * Created by hgranjal on 26/06/2018.
 */
public class Application {

    private static EJBContainer ejbContainer;

    public static void main(String[] args) {
        try {
            ejbContainer = EJBContainer.createEJBContainer();
            BookingCarService bookingCarService = lookupBookingCarService();
            System.out.println(bookingCarService.findCarAll("Marina"));
            System.out.println(bookingCarService.findCarAll("Marina"));
            System.out.println(bookingCarService.findBookingCarAll("Marina"));
            bookingCarService.remove("Marina", new BookingCar(4, "Marina"));
            System.out.println(bookingCarService.findBookingCarAll("Marina"));
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    private static BookingCarService lookupBookingCarService() throws NamingException {
        Object bookingCarService = ejbContainer.getContext().lookup("java:global/ejb-training/BookingCarServiceImpl");
        Assert.assertTrue(bookingCarService instanceof BookingCarService);
        return (BookingCarService) bookingCarService;
    }
}

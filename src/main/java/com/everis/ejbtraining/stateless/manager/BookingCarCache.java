package com.everis.ejbtraining.stateless.manager;

import com.everis.ejbtraining.stateless.model.BookingCar;
import com.everis.ejbtraining.stateless.model.Car;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by hgranjal on 26/06/2018.
 */
@ApplicationScoped
public class BookingCarCache {

    private List<Car> carList = new ArrayList<>();
    private List<BookingCar> bookingCarList = new ArrayList<>();
    private TimerTask timerTask = new TimerTask() {
        @Override
        public void run() {
            cleanCache();
        }
    };
    private Timer timer = new Timer();

    public BookingCarCache() {
        timer.scheduleAtFixedRate(timerTask, 10000, 100000);
    }

    private void cleanCache() {
        carList.clear();
        bookingCarList.clear();
    }

    public List<BookingCar> getBookingCarList() {
        return bookingCarList;
    }

    public void setBookingCarList(List<BookingCar> bookingCarList) {
        this.bookingCarList = bookingCarList;
    }

    public List<Car> getCarList() {
        return carList;
    }

    public void setCarList(List<Car> carList) {
        this.carList = carList;
    }
}

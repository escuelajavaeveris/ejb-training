package com.everis.ejbtraining.stateless.store;

import com.everis.ejbtraining.stateless.model.BookingCar;
import com.everis.ejbtraining.stateless.model.Car;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hgranjal on 26/06/2018.
 */
@ApplicationScoped
public class BookingCarStore {

    public BookingCarStore() {

    }

    public List<Car> findCarAll(Connection connection) {
        return connection.getCarList();
    }

    public List<BookingCar> findBookingCarAll(Connection connection) {
        return connection.getBookingCarList();
    }

    public void remove(BookingCar bookingCar, Connection connection) {
        connection.removeBookingCar(bookingCar);
    }

    public BookingCar book(Car car, String user, Connection connection) {
        BookingCar bookingCar = new BookingCar(car.getId(), user);
        connection.addBookingCar(bookingCar);
        return bookingCar;
    }
}

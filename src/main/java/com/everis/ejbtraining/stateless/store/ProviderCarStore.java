package com.everis.ejbtraining.stateless.store;

import com.everis.ejbtraining.stateless.model.BookingCar;
import com.everis.ejbtraining.stateless.model.Car;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 * Created by hgranjal on 26/06/2018.
 */
@ApplicationScoped
public class ProviderCarStore {

    @Inject
    private BookingCarStore bookingCarStore;

    public Car buyCar(Car car, Connection connection) {
        connection.addCar(car);
        return car;
    }
}

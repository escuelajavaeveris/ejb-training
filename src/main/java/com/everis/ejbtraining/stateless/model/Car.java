package com.everis.ejbtraining.stateless.model;

/**
 * Created by hgranjal on 26/06/2018.
 */
public class Car {
    private Integer id;
    private String brand;
    private String model;
    private String plate;

    public Car(Integer id, String brand, String model, String plate) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.plate = plate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", plate='" + plate + '\'' +
                '}';
    }
}

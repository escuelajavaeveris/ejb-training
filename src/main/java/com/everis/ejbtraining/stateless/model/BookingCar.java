package com.everis.ejbtraining.stateless.model;

/**
 * Created by hgranjal on 26/06/2018.
 */
public class BookingCar {
    private Integer idCar;
    private String userName;

    public BookingCar(Integer idCar, String userName) {
        this.idCar = idCar;
        this.userName = userName;
    }

    public Integer getIdCar() {
        return idCar;
    }

    public void setIdCar(Integer idCar) {
        this.idCar = idCar;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "BookingCar{" +
                "idCar=" + idCar +
                ", userName='" + userName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BookingCar that = (BookingCar) o;

        if (idCar != null ? !idCar.equals(that.idCar) : that.idCar != null) return false;
        return userName != null ? userName.equals(that.userName) : that.userName == null;
    }

    @Override
    public int hashCode() {
        int result = idCar != null ? idCar.hashCode() : 0;
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        return result;
    }
}

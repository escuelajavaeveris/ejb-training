package com.everis.ejbtraining.stateless.service;

import com.everis.ejbtraining.stateless.interceptor.SecurityInterceptor;
import com.everis.ejbtraining.stateless.manager.BookingCarManager;
import com.everis.ejbtraining.stateless.model.BookingCar;
import com.everis.ejbtraining.stateless.model.Car;
import com.everis.ejbtraining.stateless.store.Connection;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.util.List;

/**
 * Created by hgranjal on 26/06/2018.
 */
@Stateless
public class BookingCarServiceImpl implements BookingCarService {

    @Inject
    private BookingCarManager bookingCarManager;
    @EJB
    private ProviderCarService providerCarService;
    @EJB
    private Connection connection;

    public BookingCarServiceImpl() {
    }

    @Interceptors(SecurityInterceptor.class)
    @Override
    public List<Car> findCarAll(String username) {
        return bookingCarManager.findCarAll(connection);
    }

    @Interceptors(SecurityInterceptor.class)
    @Override
    public List<BookingCar> findBookingCarAll(String username) {
        return bookingCarManager.findBookingCarAll(connection);
    }

    @Interceptors(SecurityInterceptor.class)
    @Override
    public BookingCar book(String username, Car car, String user) {
        BookingCar bookingCar = bookingCarManager.book(car, user, connection);
        if (bookingCar == null) {
            bookingCar = bookingCarManager.book(providerCarService.buyCar(car), user, connection);
        }
        return bookingCar;
    }

    @Interceptors(SecurityInterceptor.class)
    @Override
    public void remove(String username, BookingCar bookingCar) {
        bookingCarManager.remove(bookingCar, connection);
    }
}

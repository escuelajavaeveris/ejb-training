package com.everis.ejbtraining.stateless.service;

import com.everis.ejbtraining.stateless.model.Car;

/**
 * Created by hgranjal on 26/06/2018.
 */
public interface ProviderCarService {
    Car buyCar(Car car);
}

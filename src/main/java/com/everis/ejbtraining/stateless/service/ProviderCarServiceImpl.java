package com.everis.ejbtraining.stateless.service;

import com.everis.ejbtraining.stateless.manager.ProviderCarManager;
import com.everis.ejbtraining.stateless.model.Car;
import com.everis.ejbtraining.stateless.store.Connection;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * Created by hgranjal on 26/06/2018.
 */
@Stateless
public class ProviderCarServiceImpl implements ProviderCarService {

    @Inject
    private ProviderCarManager providerCarManager;
    @EJB
    private Connection connection;

    public ProviderCarServiceImpl() {
    }

    @Override
    public Car buyCar(Car car) {
        return providerCarManager.buyCar(car, connection);
    }
}
